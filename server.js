import express from 'express';
import * as dotenv from 'dotenv';
import cors from 'cors';

//import OpenAI from 'openai';
import { Configuration, OpenAIApi } from "openai";

// Express configuration
const app = express();
app.use(cors());
app.use(express.json());


app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Internal Server Error');
});

// Use .env
dotenv.config();

// OpenAI configuration

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY
});
const openai = new OpenAIApi(configuration);


app.post('/gpttest', async (req, res) => {
  const receivedData = req.body;
  const message = receivedData.data;

  try {
    const response = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: "Answer my message: " + message,
      temperature: 0.7,
    });

    const gpt_answer = response.data.choices[0].text;
    const gpt_answer_rendered = gpt_answer.trim();
    res.json(gpt_answer_rendered);
  } catch (error) {
    console.log('Error from OpenAI:', error);
    res.json("error")
  }
  
});

app.listen(3000, () =>
  console.log("Server started at port 3000")
);
